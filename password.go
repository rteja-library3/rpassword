package rpassword

type EncryptorValidator interface {
	IsEncryptValid(password, hashedPassword []byte) (e error)
}

type EncryptorEncryption interface {
	Encrypt(password []byte) (s []byte, e error)
	String(b []byte) (s string)
}

type Encryptor interface {
	EncryptorValidator
	EncryptorEncryption
}
