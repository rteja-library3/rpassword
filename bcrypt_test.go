package rpassword_test

import (
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rpassword"
)

func TestString(t *testing.T) {
	b := []byte("password")

	bcrypt := rpassword.NewBcryptPassword(logrus.New(), 0)

	s := bcrypt.String(b)

	assert.NotEmpty(t, s, "String result should not empty")
	assert.Equal(t, "password", s, "String result should \"password\"")
}

func TestEncryptedSuccess(t *testing.T) {
	password := []byte("password")

	bcrypt := rpassword.NewBcryptPassword(logrus.New(), 0)

	encrypted, err := bcrypt.Encrypt(password)

	assert.NoError(t, err, "Password should not error")
	assert.NotEmpty(t, encrypted, "Password should not empty")
}

func TestIsEncryptValidSuccess(t *testing.T) {
	encPassword := []byte("$2a$10$lH3RZsF/k.53Bgf2u4kpLOPq99g.Z8GyQlAXyWIRSkdorkspHdjGS")
	password := []byte("password")

	bcrypt := rpassword.NewBcryptPassword(logrus.New(), 0)

	err := bcrypt.IsEncryptValid(password, encPassword)

	assert.NoError(t, err, "Password should not error")
}

func TestIsEncryptValidPasswordNotMatch(t *testing.T) {
	encPassword := []byte("$2a$10$lH3RZsF/k.53Bgf2u4kpLOPq99g.Z8GyQlAXyWIRSkdorkspHdjGS")
	password := []byte("password1")

	bcrypt := rpassword.NewBcryptPassword(logrus.New(), 0)

	err := bcrypt.IsEncryptValid(password, encPassword)

	assert.Error(t, err, "Password should error")
	assert.Equal(t, rpassword.ErrPasswordNotMatch, err, "Error should \"password not match\"")
}

func TestIsEncryptValidErrorPasswordInvalid(t *testing.T) {
	b := []byte("password")

	bcrypt := rpassword.NewBcryptPassword(logrus.New(), 0)

	err := bcrypt.IsEncryptValid(b, nil)

	assert.Error(t, err, "Password should error")
	assert.Equal(t, rpassword.ErrPasswordInvalid, err, "Error should \"invalid password\"")
}
