package rpassword

import (
	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

type bcryptPassword struct {
	logger *logrus.Logger
	cost   int
}

func NewBcryptPassword(logger *logrus.Logger, cost int) Encryptor {
	if cost == 0 {
		cost = bcrypt.DefaultCost
	}

	return &bcryptPassword{
		logger: logger,
		cost:   cost,
	}
}

// EncryptPassword do encrypt password using native bycrypt go library
func (p bcryptPassword) Encrypt(password []byte) (bytes []byte, e error) {
	bytes, e = bcrypt.GenerateFromPassword(password, p.cost)
	return
}

// IsPasswordMatchWithEncryption perform match encrypted-and-hashed password using bycrypt comparison function
func (p bcryptPassword) IsEncryptValid(password, hashedPassword []byte) (err error) {
	err = bcrypt.CompareHashAndPassword(hashedPassword, password)
	if err != nil {
		switch err {
		case bcrypt.ErrMismatchedHashAndPassword:
			err = ErrPasswordNotMatch
		case bcrypt.ErrHashTooShort:
			err = ErrPasswordInvalid
		}

		return
	}

	return
}

func (p bcryptPassword) String(b []byte) (s string) {
	return string(b)
}
