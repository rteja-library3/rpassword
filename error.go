package rpassword

import "fmt"

var (
	ErrPasswordNotMatch error = fmt.Errorf("password not match")
	ErrPasswordInvalid  error = fmt.Errorf("invalid password")
)
